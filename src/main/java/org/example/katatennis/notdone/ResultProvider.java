package org.example.katatennis.notdone;

interface ResultProvider {
  TennisResult getResult();
}
