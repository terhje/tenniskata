package org.example.katatennis.tennis1;

class NullFormatter extends ScoreFormatter {

  NullFormatter() {
    super(null, null);
  }

  @Override
  boolean isApplicable() {
    return true;
  }

  @Override
  public String toString() {
    return "";
  }
}
