package org.example.katatennis.tennis1;

class DefaultScoreFormatter extends ScoreFormatter {

  private final ScoreMapper scoreMapper;

  DefaultScoreFormatter(Player player1, Player player2, ScoreMapper scoreMapper) {
    super(player1, player2);
    this.scoreMapper = scoreMapper;
  }

  @Override
  boolean isApplicable() {
    return player1.getScore() <= 3 && player2.getScore() <= 3;
  }

  @Override
  public String toString() {
    return scoreMapper.map(player1.getScore()) + "-" + scoreMapper.map(player2.getScore());
  }
}
