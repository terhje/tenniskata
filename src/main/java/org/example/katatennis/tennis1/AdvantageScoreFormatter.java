package org.example.katatennis.tennis1;

class AdvantageScoreFormatter extends ScoreFormatter {

  AdvantageScoreFormatter(Player player1, Player player2) {
    super(player1, player2);
  }

  @Override
  boolean isApplicable() {
    return scoreIsHigherThan3() && scoreDifferenceIsOne();
  }

  @Override
  public String toString() {
    return "Advantage " + avantangePlayer().getName();
  }

  private Player avantangePlayer() {
    return player1.getScore() > player2.getScore() ? player1 : player2;
  }

  private boolean scoreDifferenceIsOne() {
    return Math.abs(player1.getScore() - player2.getScore()) == 1;
  }

  private boolean scoreIsHigherThan3() {
    return player1.getScore() > 3 || player2.getScore() > 3;
  }
}
