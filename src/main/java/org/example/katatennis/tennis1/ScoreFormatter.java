package org.example.katatennis.tennis1;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
abstract class ScoreFormatter {
  protected final Player player1;
  protected final Player player2;

  abstract boolean isApplicable();

  public abstract String toString();
}
