package org.example.katatennis.tennis1;

public interface TennisGame {
  void wonPoint(String playerName);

  String getScore();
}
