package org.example.katatennis.tennis1;

class EqualScoreFormatter extends ScoreFormatter {

  private final ScoreMapper scoreMapper;

  EqualScoreFormatter(Player player1, Player player2, ScoreMapper scoreMapper) {
    super(player1, player2);
    this.scoreMapper = scoreMapper;
  }

  @Override
  boolean isApplicable() {
    return player1.getScore() == player2.getScore();
  }

  @Override
  public String toString() {
    String score = scoreMapper.map(player1.getScore());
    return player1.getScore() > 2 ? "Deuce" : score + "-All";
  }
}
