package org.example.katatennis.tennis1;

class VictoryScoreFormatter extends ScoreFormatter {

  VictoryScoreFormatter(Player player1, Player player2) {
    super(player1, player2);
  }

  @Override
  boolean isApplicable() {
    return scoresAreHigherThanThree() && scoreDifferenceIsAboveTwo();
  }

  @Override
  public String toString() {
    return "Win for " + playerHasWon().getName();
  }

  private Player playerHasWon() {
    return player1.getScore() > player2.getScore() ? player1 : player2;
  }

  private boolean scoreDifferenceIsAboveTwo() {
    return Math.abs(player1.getScore() - player2.getScore()) >= 2;
  }

  private boolean scoresAreHigherThanThree() {
    return player1.getScore() > 3 || player2.getScore() > 3;
  }
}
