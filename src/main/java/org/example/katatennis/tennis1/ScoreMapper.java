package org.example.katatennis.tennis1;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ScoreMapper {

  static ScoreMapper getInstance() {
    return new ScoreMapper();
  }

  String map(int score) {
    return switch (score) {
      case 0 -> "Love";
      case 1 -> "Fifteen";
      case 2 -> "Thirty";
      default -> "Forty";
    };
  }
}
