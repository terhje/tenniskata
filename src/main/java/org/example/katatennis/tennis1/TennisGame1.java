package org.example.katatennis.tennis1;

class TennisGame1 implements TennisGame {

  private final Player player1;
  private final Player player2;

  private final ScoreFormatters scoreFormatters;

  TennisGame1(String player1Name, String player2Name) {
    this.player1 = new Player(player1Name);
    this.player2 = new Player(player2Name);

    scoreFormatters = new ScoreFormatters(player1, player2);
  }

  @Override
  public void wonPoint(String playerName) {
    whoScored(playerName).incrementScore();
  }

  @Override
  public String getScore() {
    return scoreFormatters.getScore();
  }

  private Player whoScored(String playerName) {
    return player1.getName().equals(playerName) ? player1 : player2;
  }
}
