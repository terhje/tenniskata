package org.example.katatennis.tennis1;

import java.util.ArrayList;
import java.util.List;

class ScoreFormatters {
  private final List<ScoreFormatter> formatters = new ArrayList<>();

  ScoreFormatters(Player player1, Player player2) {
    formatters.add(new EqualScoreFormatter(player1, player2, ScoreMapper.getInstance()));
    formatters.add(new DefaultScoreFormatter(player1, player2, ScoreMapper.getInstance()));
    formatters.add(new AdvantageScoreFormatter(player1, player2));
    formatters.add(new VictoryScoreFormatter(player1, player2));
  }

  String getScore() {
    return formatters.stream()
        .filter(ScoreFormatter::isApplicable)
        .findFirst()
        .map(ScoreFormatter::toString)
        .orElse(new NullFormatter().toString());
  }
}
