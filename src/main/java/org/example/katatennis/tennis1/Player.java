package org.example.katatennis.tennis1;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
class Player {

  private final String name;
  private int score;

  void incrementScore() {
    score++;
  }
}
